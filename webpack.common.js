const CleanWebpackPlugin = require("clean-webpack-plugin");
const path = require("path");

const config = {
    entry: "./src/index.ts",
    target: "node",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "main.js",
    },
    resolve: {
        alias: {
            uws: "ws",
        },
        extensions: [".ts", ".js"],
    },
    module: {
        rules: [{
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                enforce: "pre",
                use: [{
                    loader: "tslint-loader",
                    options: {
                        emitErrors: true,
                        failOnHint: true,
                        typeCheck: true,
                        tsConfigFile: "tsconfig.json"
                    }
                }]
            }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
    ]
};

module.exports = config;
